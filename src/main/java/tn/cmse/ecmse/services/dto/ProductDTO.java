package tn.cmse.ecmse.services.dto;
import lombok.Data;

@Data
public class ProductDTO implements Cloneable {

    private Long      idProduct ;
    private String    nameProduct ;
    private String    manufacturer ;
    private String    description ;
    private Integer   quantity ;
    private float     priceProduct ;
    private byte[]    imageProduct ;
    private Integer   discount ;


    public ProductDTO(Long idProduct,
                      String nameProduct,
                      String manufacturer,
                      String description,
                      Integer quantity,
                      float priceProduct,
                      byte[] imageProduct,
                      Integer discount) {
        this.idProduct = idProduct;
        this.nameProduct = nameProduct;
        this.manufacturer = manufacturer;
        this.description = description;
        this.quantity = quantity;
        this.priceProduct = priceProduct;
        this.imageProduct = imageProduct;
        this.discount = discount;
    }

    public ProductDTO() {}

    @Override
    public ProductDTO clone() {
        return new ProductDTO(idProduct,
                              nameProduct,
                              manufacturer,
                              description,
                              quantity,
                              priceProduct,
                              imageProduct,
                              discount);
    }
}
