package tn.cmse.ecmse.services;


import tn.cmse.ecmse.models.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    /**
     * Creates a new product
     *
     * @param  {@link Products} object to create
     * @return created {@link Product} with generated technical identifier
     */
    Product createProduct(Product product);


    /**
     * Retrieves all Products.
     *
     * @return A {@link List product} object
     */
    List<Product> getAllProducts();

    /**
     * Retrieves all Products by name.
     *
     * @return A {@link List product} object
     */
    List<Product> getAllProducts(String name);


    /**
     * Retrieves a product identified by its technical identifier.
     *
     * @param productId technical identifier of a realization
     * @return A {@link Product} object (ca va etre change en dto)
     */
    Optional<Product> getProductById(long productId);


    /**
     * updates a product.
     *
     * @param id long
     * @param product     {@link Product} object
     * @return A {@link Product} object (ca va etre change en dto)
     */
    Product updateProduct(long id, Product product);


    /**
     * deletes a choosen product.
     *
     * @return A {@link List &lt;product&gt;} object
     */
    void deleteProduct(long id);


    /**
     * deletes a choosen product.
     *
     * @return A {@link List &lt;product&gt;} object
     */
    void deleteAllProducts();

}
