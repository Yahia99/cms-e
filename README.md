# cms-e
CMSE is a Tunisian company established in 2000 (Cap-Bon Maintenance and Electrical Services) specializing in all aspects of generator sets, particularly in after-sales service.
founded by Kacem Mohssen and directed by [Kacem Chiheb][1] .
this web application is a SpringBoot based application used for e-commerce purposes
developed by [Hajri Yahia][2] & [Arfaoui Med Amine][3] .

[1]: https://www.instagram.com/chiheb_kacem/
[2]: https://www.linkedin.com/in/hajri-yahia/
[3]: https://www.linkedin.com/in/mohamed-amine-arfaoui/

```
cd existing_repo
git remote add origin https://gitlab.com/Yahia99/cms-e.git
git branch -M main
git push -uf origin main
```

