package tn.cmse.ecmse.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class ClientOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOrder ;
    private Date dateOrder ;
    private Status statusOrder ;
    private Integer quantity ;
    @OneToMany(cascade = CascadeType.ALL, mappedBy="clientOrder")
    private Set<Product> Products;

    @ManyToOne
    User users;
}
