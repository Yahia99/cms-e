package tn.cmse.ecmse.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.cmse.ecmse.models.Category;
import tn.cmse.ecmse.models.ClientOrder;
import tn.cmse.ecmse.repositories.CategoryRepository;
import tn.cmse.ecmse.services.CategoryService;
import tn.cmse.ecmse.services.OrderService;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {


    @Override
    public ClientOrder createOrder(ClientOrder order) {
        return null;
    }

    @Override
    public List<ClientOrder> getAllOrders() {
        return null;
    }

    @Override
    public List<ClientOrder> getAllOrders(String name) {
        return null;
    }

    @Override
    public Optional<ClientOrder> getorderById(long orderId) {
        return Optional.empty();
    }

    @Override
    public ClientOrder updateOrder(long id, ClientOrder order) {
        return null;
    }

    @Override
    public void deleteOrder(long id) {

    }

    @Override
    public void deleteAllOrders() {

    }
}
