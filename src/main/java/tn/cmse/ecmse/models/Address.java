package tn.cmse.ecmse.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Address {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long idAddress ;
    public String country ;
    public String city ;
    public Integer zipCode ;
    public String street ;



}
