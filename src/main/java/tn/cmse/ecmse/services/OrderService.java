package tn.cmse.ecmse.services;


import tn.cmse.ecmse.models.ClientOrder;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    /**
     * Creates a new order
     *
     * @param  {@link order} object to create
     * @return created {@link ClientOrder} with generated technical identifier
     */
    ClientOrder createOrder(ClientOrder order);


    /**
     * Retrieves all categories.
     *
     * @return A {@link List order} object
     */
    List<ClientOrder> getAllOrders();

    /**
     * Retrieves all categories by name.
     *
     * @return A {@link List order} object
     */
    List<ClientOrder> getAllOrders(String name);


    /**
     * Retrieves a order identified by its technical identifier.
     *
     * @param orderId technical identifier of a realization
     * @return A {@link ClientOrder} object (ca va etre change en dto)
     */
    Optional<ClientOrder> getorderById(long orderId);


    /**
     * updates a order.
     *
     * @param id long
     * @param order     {@link ClientOrder} object
     * @return A {@link ClientOrder} object (ca va etre change en dto)
     */
    ClientOrder updateOrder(long id, ClientOrder order);


    /**
     * deletes a choosen order.
     *
     * @return A {@link List &lt;order&gt;} object
     */
    void deleteOrder(long id);


    /**
     * deletes a choosen order.
     *
     * @return A {@link List &lt;order&gt;} object
     */
    void deleteAllOrders();

}
