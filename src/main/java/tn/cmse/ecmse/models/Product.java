package tn.cmse.ecmse.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="PRODUCT_ID")
    private Long idProduct ;

    @Column(name="PRODUCT_NAME")
    private String nameProduct ;

    @Column(name="MANUFACTURER")
    private String manufacturer ;

    @Column(name="DESCRIPTION")
    private String description ;

    @Column(name="QUANTITY")
    private Integer quantity ;

    @Column(name="PRODUCT_PRICE")
    private float priceProduct ;

    @Column(name="PRODUCT_IMAGE")
    private byte[] imageProduct ;

    @Column(name="DISCOUNT")
    private Integer discount ;

    @ManyToOne
    ClientOrder clientOrder;
}
