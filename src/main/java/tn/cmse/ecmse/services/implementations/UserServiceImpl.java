package tn.cmse.ecmse.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.cmse.ecmse.models.User;
import tn.cmse.ecmse.repositories.UserRepository;
import tn.cmse.ecmse.services.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<User>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public Optional<User> getUserById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public User updateUser(User userTobeUpdated, User updatedUser) {
        if (updatedUser.getFirstName() != null) userTobeUpdated.setFirstName(updatedUser.getFirstName());
        if (updatedUser.getLastName() != null) userTobeUpdated.setLastName(updatedUser.getLastName());
        if (updatedUser.getEmail() != null) userTobeUpdated.setEmail(updatedUser.getEmail());
        if (updatedUser.getPhone() != null) userTobeUpdated.setPhone(updatedUser.getPhone());
        if (updatedUser.getDateOfBirth() != null) userTobeUpdated.setDateOfBirth(updatedUser.getDateOfBirth());
        if (updatedUser.getPassword() != null) userTobeUpdated.setPassword(updatedUser.getPassword());
        if (updatedUser.getGender() != null) userTobeUpdated.setGender(updatedUser.getGender());
        return updatedUser;
    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }
}
