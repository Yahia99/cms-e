package tn.cmse.ecmse.services.dto;

import tn.cmse.ecmse.models.Gender;
import tn.cmse.ecmse.models.Role;

import java.util.Date;
import lombok.Data;

@Data
public class UserDTO implements Cloneable {
    private  Long      id;
    private  String    firstName;
    private  String    lastName;
    private  String    mail;
    private  String    password;
    private  Gender    gender;
    private  Integer   phone;
    private  Date      dateOfBirth;
    private  Role      role;

    public UserDTO(Long id,
                   String firstName,
                   String lastName,
                   String mail,
                   String password,
                   Gender gender,
                   Integer phone,
                   Date dateOfBirth,
                   Role role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
        this.gender = gender;
        this.phone = phone;
        this.dateOfBirth = dateOfBirth;
        this.role = role;
    }

    public UserDTO() {}

    @Override
    public UserDTO clone() {
        return new UserDTO(id,
                           firstName,
                           lastName,
                           mail,
                           password,
                           gender,
                           phone,
                           dateOfBirth,
                           role);
    }
}
