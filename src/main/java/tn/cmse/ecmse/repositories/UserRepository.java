package tn.cmse.ecmse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.cmse.ecmse.models.User;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUserName(String firstName);

    Boolean existsByUserName(String firstName);

    Boolean existsByEmail(String email);
}
