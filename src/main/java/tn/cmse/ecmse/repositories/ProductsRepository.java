package tn.cmse.ecmse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.cmse.ecmse.models.Product;

@Repository
public interface ProductsRepository extends JpaRepository<Product, Long> {
}
