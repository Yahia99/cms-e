package tn.cmse.ecmse.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long categoryId ;
    public String categoryName ;

    public Category(String categoryName) {
    }

    public Category() {

    }
}
