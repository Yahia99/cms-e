package tn.cmse.ecmse.services.implementations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tn.cmse.ecmse.models.Category;
import tn.cmse.ecmse.repositories.CategoryRepository;
import tn.cmse.ecmse.services.CategoryService;

import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {


    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public Category createCategory(Category category) {
        return categoryRepository
                .save(new Category(category.getCategoryName()));
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public List<Category> getAllCategories(String name) {
        return categoryRepository.findByCategoryNameContaining(name);
    }

    @Override
    public Optional<Category> getCategoryById(long CategoryId) {
        return categoryRepository.findById(CategoryId);
    }

    @Override
    public Category updateCategory(long id, Category category) {
        Optional<Category> categoryData = categoryRepository.findById(id);
        if (categoryData.isPresent()) {
            Category _category = categoryData.get();
            _category.setCategoryName(category.getCategoryName());
            categoryRepository.save(_category);
            return _category;
        } else {
            return null;
        }
    }

    @Override
    public void deleteCategory(long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public void deleteAllCategories() {
        categoryRepository.deleteAll();
    }
}
