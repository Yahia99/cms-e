package tn.cmse.ecmse.services;


import tn.cmse.ecmse.models.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryService {

    /**
     * Creates a new category
     *
     * @param  {@link Category} object to create
     * @return created {@link Category} with generated technical identifier
     */
    Category createCategory(Category category);


    /**
     * Retrieves all categories.
     *
     * @return A {@link List Category} object
     */
    List<Category> getAllCategories();

    /**
     * Retrieves all categories by name.
     *
     * @return A {@link List Category} object
     */
    List<Category> getAllCategories(String name);


    /**
     * Retrieves a Category identified by its technical identifier.
     *
     * @param CategoryId technical identifier of a realization
     * @return A {@link Category} object (ca va etre change en dto)
     */
    Optional<Category> getCategoryById(long CategoryId);


    /**
     * updates a Category.
     *
     * @param id long
     * @param category     {@link Category} object
     * @return A {@link Category} object (ca va etre change en dto)
     */
    Category updateCategory(long id, Category category);


    /**
     * deletes a choosen Category.
     *
     * @return A {@link List &lt;Category&gt;} object
     */
    void deleteCategory(long id);


    /**
     * deletes a choosen Category.
     *
     * @return A {@link List &lt;Category&gt;} object
     */
    void deleteAllCategories();

}
