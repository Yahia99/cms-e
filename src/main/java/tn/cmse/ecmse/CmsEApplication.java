package tn.cmse.ecmse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CmsEApplication {

    public static void main(String[] args) {
        SpringApplication.run(CmsEApplication.class, args);
    }

}
