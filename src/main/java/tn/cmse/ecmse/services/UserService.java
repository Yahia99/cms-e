package tn.cmse.ecmse.services;

import tn.cmse.ecmse.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    /**
     * Creates a new user
     *
     * @param user {@link User} object to create
     * @return created {@link User} with generated technical identifier
     */
    User createUser(User user);


    /**
     * Retrieves all Realizations by Filter.
     *
     * @return A {@link List &lt;User&gt;} object
     */
    List<User> getAllUsers();


    /**
     * Retrieves a user identified by its technical identifier.
     *
     * @param userId technical identifier of a realization
     * @return A {@link User} object (ca va etre change en dto)
     */
    Optional<User> getUserById(long userId);


    /**
     * updates a user.
     *
     * @param userTobeUpdated {@link User} object
     * @param updatedUser     {@link User} object
     * @return A {@link User} object (ca va etre change en dto)
     */
    User updateUser(User userTobeUpdated, User updatedUser);


    /**
     * deletes a choosen user.
     *
     * @return A {@link List &lt;User&gt;} object
     */
    void deleteUser(long id);

}
