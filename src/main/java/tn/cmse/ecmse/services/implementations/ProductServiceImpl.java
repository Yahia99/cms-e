package tn.cmse.ecmse.services.implementations;

import org.springframework.stereotype.Service;
import tn.cmse.ecmse.models.Product;
import tn.cmse.ecmse.services.ProductService;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Override
    public Product createProduct(Product product) {
        return null;
    }

    @Override
    public List<Product> getAllProducts() {
        return null;
    }

    @Override
    public List<Product> getAllProducts(String name) {
        return null;
    }

    @Override
    public Optional<Product> getProductById(long productId) {
        return Optional.empty();
    }

    @Override
    public Product updateProduct(long id, Product product) {
        return null;
    }

    @Override
    public void deleteProduct(long id) {

    }

    @Override
    public void deleteAllProducts() {

    }
}
